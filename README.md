##### COMMANDS

###### list commands:
menu  
stock  
orders  

###### Add commands:
+menu  
+stock  
+order  

###### Delete commands:
-menu  
-stock  
-order  

###### Update commands:
/menu  
/stock  
/order  