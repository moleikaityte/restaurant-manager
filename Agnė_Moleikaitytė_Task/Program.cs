﻿using Agnė_Moleikaitytė_Task.Controllers;
using Agnė_Moleikaitytė_Task.Data;
using Agnė_Moleikaitytė_Task.Dto;
using Agnė_Moleikaitytė_Task.Repositories;
using Agnė_Moleikaitytė_Task.Repositories.IRepositories;
using Agnė_Moleikaitytė_Task.Services;
using Agnė_Moleikaitytė_Task.Services.IServices;
using System;
using System.Linq;

namespace Agnė_Moleikaitytė_Task
{
    class Program
    {
        private static MenuController MenuController;
        private static OrderController OrderController;
        private static StockController StockController;

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Restaurant Manager");
            Console.WriteLine("see command list in README.md\n");
            Configure();
            Start();
        }

        private static void Start()
        {
            var inputValue = Console.ReadLine();
            while (inputValue != "exit")
            {
                switch (inputValue)
                {
                    case "menu":
                        Console.WriteLine("ALL MENU ITEMS:");
                        Menu();
                        break;
                    case "stock":
                        Console.WriteLine("ALL STOCK ITEMS:");
                        Stock();
                        break;
                    case "orders":
                        Console.WriteLine("ALL ORDERS:");
                        Orders();
                        break;
                    case "+stock":
                        Console.WriteLine("ADD NEW STOCK ITEM");
                        AddStock();
                        break;
                    case "-stock":
                        Console.WriteLine("DELETE STOCK ITEM ");
                        DeleteStock();
                        break;
                    case "/stock":
                        Console.WriteLine("UPDATE STOCK ITEM");
                        UpdateStock();
                        break;
                    case "+menu":
                        Console.WriteLine("ADD MENU ITEM");
                        AddMenu();
                        break;
                    case "-menu":
                        Console.WriteLine("DELETE MENU ITEM");
                        DeleteMenu();
                        break;
                    case "/menu":
                        Console.WriteLine("UPDATE MENU ITEM");
                        UpdateMenu();
                        break;
                    case "+order":
                        Console.WriteLine("ADD ORDER");
                        AddOrder();
                        break;
                    case "-order":
                        Console.WriteLine("DELETE ORDER");
                        DeleteOrder();
                        break;
                    case "/order":
                        Console.WriteLine("UPDATE ORDER");
                        UpdateOrder();
                        break;
                    default:
                        Console.WriteLine("unknown command");
                        break;
                }
                Console.WriteLine();
                inputValue = Console.ReadLine();
            }
            Environment.Exit(0);
        }

        private static void Configure()
        {
            IMenuRepository menuRepository = new MenuRepository();
            IOrderRepository orderRepository = new OrderRepository();
            IStockRepository stockRepository = new StockRepository();

            IStockService stockService = new StockService(stockRepository);
            IMenuService menuService = new MenuService(menuRepository, stockService);
            IOrderService orderService = new OrderService(orderRepository, menuService, stockService);

            MenuController = new MenuController(menuService);
            OrderController = new OrderController(orderService);
            StockController = new StockController(stockService);
        }

        public static void Menu()
        {
            PrintMenuItemHeader();
            var menu = MenuController.FindAll();
            foreach(var m in menu)
            {
                Console.WriteLine(m.ToString());
            }
        }

        public static void Orders()
        {
            PrintOrderHeader();
            var orders = OrderController.FindAll();
            foreach (var o in orders)
            {
                Console.WriteLine(o.ToString());
            }
        }

        public static void Stock()
        {
            PrintStockItemHeader();
            var stock = StockController.FindAll();
            foreach (var s in stock)
            {
                Console.WriteLine(s.ToString());
            }
        }

        public static void AddStock()
        {
            var stockItem = new StockItemDto();
            Console.WriteLine("stock item name:");
            stockItem.Name = Console.ReadLine();

            Console.WriteLine("stock item unit type: \n0 - kg\n1 - l");
            int unitIndex = int.Parse(Console.ReadLine());
            stockItem.Unit = (EUnit)unitIndex;

            Console.WriteLine("stock item portion size");
            stockItem.PortionSize = double.Parse(Console.ReadLine());

            Console.WriteLine("do you want to add product's quantity by:\n0 - size\n1 - portions count");
            stockItem.IsPortionIndivisible = Convert.ToBoolean(int.Parse(Console.ReadLine()));

            Console.WriteLine("stock item size/count:");
            stockItem.Size = double.Parse(Console.ReadLine());

            var item = StockController.Add(stockItem);
            if (item != null)
            {
                Console.WriteLine("Item added successfully");
                PrintStockItemHeader();
                Console.WriteLine(item.ToString());
            }
        }

        public static void DeleteStock()
        {
            Console.WriteLine("stock item id to remove:");
            var stockItemId = int.Parse(Console.ReadLine());
            StockController.Delete(stockItemId);
        }

        public static void UpdateStock()
        {
            Console.WriteLine("stock item id to update:");
            var stockItemId = int.Parse(Console.ReadLine());

            var item = StockController.FindById(stockItemId);
            if (item != null)
            {
                Console.WriteLine("Item to be updated:");
                PrintStockItemHeader();
                Console.WriteLine(item.ToString());
            }

            var stockItem = new StockItemDto();
            Console.WriteLine("stock item name:");
            stockItem.Name = Console.ReadLine();

            Console.WriteLine("stock item unit type: \n0 - kg\n1 - l");
            int unitIndex = int.Parse(Console.ReadLine());
            stockItem.Unit = (EUnit)unitIndex;

            Console.WriteLine("stock item portion size");
            stockItem.PortionSize = double.Parse(Console.ReadLine());

            Console.WriteLine("do you want to add product's quantity by:\n0 - size\n1 - portions count");
            stockItem.IsPortionIndivisible = Convert.ToBoolean(int.Parse(Console.ReadLine()));

            Console.WriteLine("stock item size/count:");
            stockItem.Size = double.Parse(Console.ReadLine());

            item = StockController.Update(stockItemId, stockItem);
            if (item != null)
            {
                Console.WriteLine("Item updated successfully");
                PrintStockItemHeader();
                Console.WriteLine(item.ToString());
            }
        }

        public static void AddMenu()
        {
            var menuItem = new MenuItemDto();
            Console.WriteLine("menu item name:");
            menuItem.Name = Console.ReadLine();

            Console.WriteLine("menu item product ids (separated by space):");
            menuItem.ProductIds = Console.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToHashSet();

            var item = MenuController.Add(menuItem);
            if (item != null)
            {
                Console.WriteLine("Item added successfully");
                PrintMenuItemHeader();
                Console.WriteLine(item.ToString());
            }
        }

        public static void DeleteMenu()
        {
            Console.WriteLine("menu item id to remove:");
            var menuItemId = int.Parse(Console.ReadLine());
            MenuController.Delete(menuItemId);
        }

        public static void UpdateMenu()
        {
            Console.WriteLine("menu item id to update:");
            var menuItemId = int.Parse(Console.ReadLine());

            var item = MenuController.FindById(menuItemId);
            if (item != null)
            {
                Console.WriteLine("Item to be updated:");
                PrintMenuItemHeader();
                Console.WriteLine(item.ToString());
            }

            var menuItem = new MenuItemDto();
            Console.WriteLine("menu item name:");
            menuItem.Name = Console.ReadLine();

            Console.WriteLine("menu item product ids (separated by space):");
            menuItem.ProductIds = Console.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToHashSet();

            item = MenuController.Update(menuItemId, menuItem);
            if (item != null)
            {
                Console.WriteLine("Item updated successfully");
                PrintMenuItemHeader();
                Console.WriteLine(item.ToString());
            }
        }

        public static void AddOrder()
        {
            var order = new OrderDto();

            Console.WriteLine("order menu item ids: (separated by space):");
            order.MenuItemIds = Console.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList();

            var addedOrder = OrderController.Add(order);
            if (addedOrder != null)
            {
                Console.WriteLine("order added successfully");
                PrintOrderHeader();
                Console.WriteLine(addedOrder.ToString());
            }
        }

        public static void DeleteOrder()
        {
            Console.WriteLine("order id to remove:");
            var orderId = int.Parse(Console.ReadLine());
            OrderController.Delete(orderId);
        }

        public static void UpdateOrder()
        {
            Console.WriteLine("order id to update:");
            var orderId = int.Parse(Console.ReadLine());

            var order = OrderController.FindById(orderId);
            if (order != null)
            {
                Console.WriteLine("order to be updated:");
                PrintOrderHeader();
                Console.WriteLine(order.ToString());
            }

            var orderUpdate = new OrderDto();
            Console.WriteLine("order menu item ids: (separated by space):");
            orderUpdate.MenuItemIds = Console.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList();

            order = OrderController.Update(orderId, orderUpdate);
            if (order != null)
            {
                Console.WriteLine("order updated successfully");
                PrintOrderHeader();
                Console.WriteLine(order.ToString());
            }
        }


        public static void PrintMenuItemHeader()
        {
            Console.WriteLine(new string('_', 48));
            Console.WriteLine(string.Format("|{0,-3}|{1,-30}|{2,11}|", "Id", "Name", "Product Ids"));
            Console.WriteLine(new string('_', 48));
        }

        public static void PrintStockItemHeader()
        {
            Console.WriteLine(new string('_', 59));
            Console.WriteLine(string.Format("|{0,-3}|{1,-20}|{2,-13}|{3,-5}|{4,-12}|", "Id", "Name", "Portion Count", "Unit", "Portion Size"));
            Console.WriteLine(new string('_', 59));
        }

        public static void PrintOrderHeader()
        {
            Console.WriteLine(new string('_', 40));
            Console.WriteLine(string.Format("|{0,-3}|{1,-20}|{2,13}|", "Id", "DateTime", "Menu Item Ids"));
            Console.WriteLine(new string('_', 40));
        }
    }
}
