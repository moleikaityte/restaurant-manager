﻿using Agnė_Moleikaitytė_Task.Constants;
using Agnė_Moleikaitytė_Task.Data;
using CsvHelper.Configuration;

namespace Agnė_Moleikaitytė_Task.Mappers
{
    class CsvStockMap : ClassMap<StockItem>
    {
        public CsvStockMap()
        {
            Map(m => m.Id).Name(CsvStockHeaders.Id);
            Map(m => m.Name).Name(CsvStockHeaders.Name);
            Map(m => m.PortionCount).Name(CsvStockHeaders.PortionCount);
            Map(m => m.PortionSize).Name(CsvStockHeaders.PortionSize);
            Map(m => m.Unit).Name(CsvStockHeaders.Unit);
        }
    }
}
