﻿using Agnė_Moleikaitytė_Task.Constants;
using Agnė_Moleikaitytė_Task.Data;
using CsvHelper.Configuration;

namespace Agnė_Moleikaitytė_Task.Mappers
{
    sealed class CsvOrderMap : ClassMap<Order>
    {
        public CsvOrderMap()
        {
            Map(m => m.Id).Name(CsvOrderHeaders.Id);
            Map(m => m.DateTime).Name(CsvOrderHeaders.DateTime);
            Map(m => m.MenuItemIds).Name(CsvOrderHeaders.MenuItems);
        }
    }
}
