﻿using Agnė_Moleikaitytė_Task.Constants;
using Agnė_Moleikaitytė_Task.Data;
using CsvHelper.Configuration;

namespace Agnė_Moleikaitytė_Task.Mappers
{
    sealed class CsvMenuItemMap : ClassMap<MenuItem>
    {
        public CsvMenuItemMap()
        {
            Map(m => m.Id).Name(CsvMenuHeaders.Id);
            Map(m => m.Name).Name(CsvMenuHeaders.Name);
            Map(m => m.ProductIds).Name(CsvMenuHeaders.Products);
        }
    }
}
