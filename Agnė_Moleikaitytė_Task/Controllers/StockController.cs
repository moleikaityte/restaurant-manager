﻿using Agnė_Moleikaitytė_Task.Data;
using Agnė_Moleikaitytė_Task.Dto;
using Agnė_Moleikaitytė_Task.Services.IServices;
using System;
using System.Collections.Generic;

namespace Agnė_Moleikaitytė_Task.Controllers
{
    class StockController
    {
        private readonly IStockService _stockService;

        public StockController(IStockService stockService)
        {
            _stockService = stockService;
        }

        public StockItem Add(StockItemDto stockItemRequest)
        {
            var stockItem = _stockService.Add(stockItemRequest);
            if (stockItem == null)
            {
                Console.WriteLine("Bad request");
            }
            else
            {
                Console.WriteLine(string.Format("Stock item ({0}) added successfully with id = {1}", stockItem.Name, stockItem.Id));
            }
            return stockItem;
        }

        public StockItem FindById(int id)
        {
            var stockItem = _stockService.FindById(id);
            if (stockItem == null)
            {
                Console.WriteLine(string.Format("Stock item with id = {0} was not found", stockItem.Id));
            }
            return stockItem;
        }

        public HashSet<StockItem> FindAll()
        {
            var stockItems = _stockService.FindAll();
            return stockItems;
        }

        public bool Delete(int id)
        {
            var stockItem = _stockService.Delete(id);
            if (stockItem == false)
            {
                Console.WriteLine(string.Format("Stock item with id = {0} was not found", id));
            }
            else
            {
                Console.WriteLine("Stock item with id = {0} deleted successfully", id);
            }
            return stockItem;
        }

        public StockItem Update(int id, StockItemDto stockItemRequest)
        {
            var stockItem = _stockService.Update(id, stockItemRequest);
            if (stockItem == null)
            {
                Console.WriteLine(string.Format("Stock item with id = {0} was not found", id));
            }
            return stockItem;
        }
    }
}
