﻿using Agnė_Moleikaitytė_Task.Data;
using Agnė_Moleikaitytė_Task.Dto;
using Agnė_Moleikaitytė_Task.Services.IServices;
using System;
using System.Collections.Generic;

namespace Agnė_Moleikaitytė_Task.Controllers
{
    class OrderController
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        public Order Add(OrderDto orderRequest)
        {
            var order = _orderService.Add(orderRequest);
            if (order == null)
            {
                Console.WriteLine("Bad request");
            }
            else
            {
                Console.WriteLine(string.Format("Order made {0} added successfully with id = {1}", order.DateTime, order.Id));
            }
            return order;
        }

        public Order FindById(int id)
        {
            var order = _orderService.FindById(id);
            if (order == null)
            {
                Console.WriteLine(string.Format("Order with id = {0} was not found", order.Id));
            }
            return order;
        }

        public HashSet<Order> FindAll()
        {
            var orders = _orderService.FindAll();
            return orders;
        }

        public bool Delete(int id)
        {
            var order = _orderService.Delete(id);
            if (order == false)
            {
                Console.WriteLine(string.Format("Order with id = {0} was not found", id));
            }
            else
            {
                Console.WriteLine("Order with id = {0} deleted successfully", id);
            }
            return order;
        }

        public Order Update(int id, OrderDto orderRequest)
        {
            var order = _orderService.Update(id, orderRequest);
            if (order == null)
            {
                Console.WriteLine(string.Format("Order with id = {0} was not found", id));
            }
            return order;
        }
    }
}
