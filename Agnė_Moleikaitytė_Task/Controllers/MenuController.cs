﻿using Agnė_Moleikaitytė_Task.Data;
using Agnė_Moleikaitytė_Task.Dto;
using Agnė_Moleikaitytė_Task.Services.IServices;
using System;
using System.Collections.Generic;

namespace Agnė_Moleikaitytė_Task.Controllers
{
    class MenuController
    {
        private readonly IMenuService _menuService;

        public MenuController(IMenuService menuService)
        {
            _menuService = menuService;
        }

        public MenuItem Add(MenuItemDto menuItemRequest)
        {
            var menuItem = _menuService.Add(menuItemRequest);
            if (menuItem == null)
            {
                Console.WriteLine("Bad request");
            }
            else
            {
                Console.WriteLine(string.Format("Menu item ({0}) added successfully with id = {1}", menuItem.Name, menuItem.Id));
            }
            return menuItem;
        }

        public MenuItem FindById(int id)
        {
            var menuItem = _menuService.FindById(id);
            if (menuItem == null)
            {
                Console.WriteLine(string.Format("Menu item with id = {0} was not found", menuItem.Id));
            }
            return menuItem;
        }

        public HashSet<MenuItem> FindAll()
        {
            var menuItems = _menuService.FindAll();
            return menuItems;
        }

        public bool Delete(int id)
        {
            var menuItem = _menuService.Delete(id);
            if (menuItem == false)
            {
                Console.WriteLine(string.Format("Menu item with id = {0} was not found", id));
            }
            else
            {
                Console.WriteLine("Menu item with id = {0} deleted successfully", id);
            }
            return menuItem;
        }

        public MenuItem Update(int id, MenuItemDto menuItemRequest)
        {
            var menuItem = _menuService.Update(id, menuItemRequest);
            if (menuItem == null)
            {
                Console.WriteLine(string.Format("Menu item with id = {0} was not found", id));
            }
            return menuItem;
        }
    }
}
