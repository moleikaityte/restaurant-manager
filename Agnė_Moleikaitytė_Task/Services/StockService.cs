﻿using Agnė_Moleikaitytė_Task.Data;
using Agnė_Moleikaitytė_Task.Dto;
using Agnė_Moleikaitytė_Task.Repositories.IRepositories;
using Agnė_Moleikaitytė_Task.Services.IServices;
using System.Collections.Generic;
using System.Linq;

namespace Agnė_Moleikaitytė_Task.Services
{
    public class StockService : IStockService
    {
        private readonly IStockRepository _stockRepository;

        public StockService(IStockRepository stockRepository)
        {
            _stockRepository = stockRepository;
        }

        public StockItem Add(StockItemDto stockItemRequest)
        {
            var stockItem = new StockItem
            {
                Name = stockItemRequest.Name,
                PortionSize = stockItemRequest.PortionSize,
                Unit = stockItemRequest.Unit
            };

            if (stockItemRequest.IsPortionIndivisible)
            {
                stockItem.PortionCount = stockItemRequest.Size;
            }
            else
            {
                stockItem.PortionCount = stockItemRequest.Size / stockItem.PortionSize;
            }

            return _stockRepository.Add(stockItem);
        }

        public bool Delete(int id)
        {
            return _stockRepository.Delete(id);
        }

        public HashSet<StockItem> FindAll()
        {
            return _stockRepository.FindAll();
        }

        public StockItem FindById(int id)
        {
            return _stockRepository.FindById(id);
        }



        public bool IsEnoughStockForOrder(IDictionary<StockItem, int> stockItems)
        {
            return !stockItems
                .Select(item => item.Key.PortionCount >= item.Value)
                .Any(x => x == false);
        }

        public StockItem Update(int id, StockItemDto stockItemRequest)
        {
            var stockItem = FindById(id);
            if (stockItem == null)
            {
                return null;
            }

            stockItem.Name = stockItemRequest.Name;
            stockItem.PortionSize = stockItemRequest.PortionSize;
            stockItem.Unit = stockItemRequest.Unit;

            if (stockItemRequest.IsPortionIndivisible)
            {
                stockItem.PortionCount = stockItemRequest.Size;
            }
            else
            {
                stockItem.PortionCount = stockItemRequest.Size / stockItem.PortionSize;
            }

            return _stockRepository.Update(stockItem);
        }

        public bool RemoveOrderProductsFromStock(IDictionary<StockItem, int> stockItems)
        {
            if (!IsEnoughStockForOrder(stockItems))
            {
                return false;
            }

            return !stockItems.Select(stockItem =>
            {
                stockItem.Key.PortionCount -= stockItem.Value;
                return _stockRepository.Update(stockItem.Key);
            }).Any(x => x == null);
        }

        public void ReturnStockItems(IDictionary<StockItem, int> oldStockItems)
        {
            foreach (var item in oldStockItems)
            {
                var stockItem = _stockRepository.FindById(item.Key.Id);
                if (stockItem != null)
                {
                    stockItem.PortionCount += item.Value;
                    _stockRepository.Update(stockItem);
                }
            }
        }
    }
}
