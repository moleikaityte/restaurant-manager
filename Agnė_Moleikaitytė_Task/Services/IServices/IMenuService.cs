﻿using Agnė_Moleikaitytė_Task.Data;
using Agnė_Moleikaitytė_Task.Dto;
using System.Collections.Generic;

namespace Agnė_Moleikaitytė_Task.Services.IServices
{
    public interface IMenuService
    {
        public MenuItem Add(MenuItemDto menuItem);
        public MenuItem FindById(int id);
        public HashSet<MenuItem> FindAll();
        public bool Delete(int id);
        public MenuItem Update(int id, MenuItemDto menuItemRequest);
    }
}
