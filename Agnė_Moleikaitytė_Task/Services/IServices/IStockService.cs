﻿using Agnė_Moleikaitytė_Task.Data;
using Agnė_Moleikaitytė_Task.Dto;
using System.Collections.Generic;

namespace Agnė_Moleikaitytė_Task.Services.IServices
{
    public interface IStockService
    {
        StockItem Add(StockItemDto stockItem);
        StockItem FindById(int id);
        HashSet<StockItem> FindAll();
        bool Delete(int id);
        StockItem Update(int id, StockItemDto stockItem);
        bool RemoveOrderProductsFromStock(IDictionary<StockItem, int> stockItems);
        void ReturnStockItems(IDictionary<StockItem, int> oldStockItems);
    }
}
