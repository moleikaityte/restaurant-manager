﻿using Agnė_Moleikaitytė_Task.Data;
using Agnė_Moleikaitytė_Task.Dto;
using System.Collections.Generic;

namespace Agnė_Moleikaitytė_Task.Services.IServices
{
    public interface IOrderService
    {
        public Order Add(OrderDto order);
        public Order FindById(int id);
        public HashSet<Order> FindAll();
        public bool Delete(int id);
        public Order Update(int id, OrderDto order);
    }
}
