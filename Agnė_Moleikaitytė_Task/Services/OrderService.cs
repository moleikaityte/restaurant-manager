﻿using Agnė_Moleikaitytė_Task.Data;
using Agnė_Moleikaitytė_Task.Dto;
using Agnė_Moleikaitytė_Task.Repositories.IRepositories;
using Agnė_Moleikaitytė_Task.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Agnė_Moleikaitytė_Task.Services
{
    class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;

        private readonly IMenuService _menuService;
        private readonly IStockService _stockService;

        public OrderService(IOrderRepository orderRepository, 
                            IMenuService menuService, 
                            IStockService stockService)
        {
            _orderRepository = orderRepository;
            _menuService = menuService;
            _stockService = stockService;
        }

        public Order Add(OrderDto orderRequest)
        {
            var stockItems = GetStockItemsForOrder(orderRequest.MenuItemIds);
            if (stockItems == null)
            {
                return null;
            }

            if (!_stockService.RemoveOrderProductsFromStock(stockItems))
            {
                return null;
            }

            var order = new Order();
            order.SetMenuItemIds(orderRequest.MenuItemIds);
            order.DateTime = DateTime.Now;

            return _orderRepository.Add(order);
        }

        public bool Delete(int id)
        {
            return _orderRepository.Delete(id);
        }

        public HashSet<Order> FindAll()
        {
            return _orderRepository.FindAll();
        }

        public Order FindById(int id)
        {
            return _orderRepository.FindById(id);
        }

        public Order Update(int id, OrderDto orderRequest)
        {
            var order = _orderRepository.FindById(id);
            if (order == null)
            {
                return null;
            }

            var oldStockItems = GetStockItemsForOrder(order.MenuItemIds.Split(' ').Select(int.Parse).ToList());
            if (oldStockItems == null)
            {
                return null;
            }
            _stockService.ReturnStockItems(oldStockItems);

            var stockItems = GetStockItemsForOrder(orderRequest.MenuItemIds);
            if (stockItems == null)
            {
                return null;
            }

            _stockService.RemoveOrderProductsFromStock(stockItems);
            order.SetMenuItemIds(orderRequest.MenuItemIds);
            order.DateTime = DateTime.Now;

            return _orderRepository.Update(order);
        }

        private IDictionary<StockItem, int> GetStockItemsForOrder(IList<int> menuItemIds)
        {
            IDictionary<StockItem, int> orderStockItems = new Dictionary<StockItem, int>();

            var menuItems = menuItemIds // iterate over menu items
                .Select(menuItemId =>
                {
                    var menuItem = _menuService.FindById(menuItemId);
                    if (menuItem == null)
                    {
                        return null;
                    }

                    
                    var stockItems = menuItem.ProductIds.Split(' ').Select(int.Parse).ToHashSet() // iterate over stock items
                        .Select(productId =>
                        {
                            var stockItem = _stockService.FindById(productId);
                            if (stockItem == null)
                            {
                                return null;
                                
                            }

                            if(orderStockItems.ContainsKey(stockItem)) // set/add stock item count
                            {
                                orderStockItems[stockItem]++;
                            }
                            else
                            {
                                orderStockItems.Add(stockItem, 1);
                            }
                            return stockItem;
                        });

                    if (stockItems.Any(x => x == null))
                    {
                        return null;
                    }
                    return menuItem;
                });

            if (menuItems.Any(x => x == null))
            {
                return null;
            }

            return orderStockItems;
        }
    }
}
