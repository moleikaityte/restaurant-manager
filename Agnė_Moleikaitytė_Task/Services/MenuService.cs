﻿using Agnė_Moleikaitytė_Task.Data;
using Agnė_Moleikaitytė_Task.Dto;
using Agnė_Moleikaitytė_Task.Repositories.IRepositories;
using Agnė_Moleikaitytė_Task.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Agnė_Moleikaitytė_Task.Services
{
    sealed class MenuService : IMenuService
    {
        private readonly IMenuRepository _menuRepository;
        private readonly IStockService _stockService;

        public MenuService(IMenuRepository menuRepository,
                           IStockService stockService)
        {
            _menuRepository = menuRepository;
            _stockService = stockService;
        }

        public MenuItem Add(MenuItemDto menuItemRequest)
        {
            if (!AreMenuProductIdsValid(menuItemRequest.ProductIds))
            {
                return null;
            }

            var menuItem = new MenuItem
            {
                Name = menuItemRequest.Name 
            };
            menuItem.SetProductIds(menuItemRequest.ProductIds);

            return _menuRepository.Add(menuItem);
        }

        public bool Delete(int id)
        {
            return _menuRepository.Delete(id);
        }

        public HashSet<MenuItem> FindAll()
        {
            return _menuRepository.FindAll();
        }

        public MenuItem FindById(int id)
        {
            return _menuRepository.FindById(id);
        }

        public MenuItem Update(int id, MenuItemDto menuItemRequest)
        {
            var menuItem = FindById(id);
            if (menuItem == null)
            {
                return null;
            }

            if (!AreMenuProductIdsValid(menuItemRequest.ProductIds))
            {
                return null;
            }

            menuItem.Name = menuItemRequest.Name;
            menuItem.SetProductIds(menuItemRequest.ProductIds);

            return _menuRepository.Update(id, menuItem);
        }

        private bool AreMenuProductIdsValid(HashSet<int> ids)
        {
            var missingStockItems = ids
                .Where(productId => _stockService.FindById(productId) == null)
                .Count();

            var multipleIds = ids
                .GroupBy(productId => productId)
                .Where(id => id.Count() > 1)
                .Select(id => id.Key)
                .Count();

            if (missingStockItems > 0 || multipleIds > 0)
            {
                return false;
            }
            return true;
        }
    }
}
