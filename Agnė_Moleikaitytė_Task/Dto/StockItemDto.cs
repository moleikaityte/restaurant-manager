﻿using Agnė_Moleikaitytė_Task.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agnė_Moleikaitytė_Task.Dto
{
    public class StockItemDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public double Size { get; set; }
        public EUnit Unit { get; set; }
        public double PortionSize { get; set; }
        public bool IsPortionIndivisible { get; set; } = false; // user puts in portions count instead of stock item size
    }
}
