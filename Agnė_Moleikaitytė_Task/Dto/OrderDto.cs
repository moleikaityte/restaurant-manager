﻿using System;
using System.Collections.Generic;

namespace Agnė_Moleikaitytė_Task.Dto
{
    public class OrderDto
    {
        public long Id { get; set; }
        public DateTime DateTime { get; private set; } = DateTime.Now;
        public IList<int> MenuItemIds { get; set; }
    }
}
