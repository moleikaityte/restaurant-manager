﻿using System.Collections.Generic;

namespace Agnė_Moleikaitytė_Task.Dto
{
    public class MenuItemDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public HashSet<int> ProductIds { get; set; }
    }
}
