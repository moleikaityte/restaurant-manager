﻿using Agnė_Moleikaitytė_Task.Constants;
using Agnė_Moleikaitytė_Task.Data;
using Agnė_Moleikaitytė_Task.Mappers;
using Agnė_Moleikaitytė_Task.Repositories.IRepositories;
using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace Agnė_Moleikaitytė_Task.Repositories
{
    public class StockRepository : IStockRepository
    {
        /// <summary>
        /// Method adds new stock item
        /// </summary>
        /// <param name="stockItem">stock item to add</param>
        /// <returns>added stock item</returns>
        public StockItem Add(StockItem stockItem)
        {
            var nextId = FindAll().Max(x => x.Id) + 1;
            stockItem.Id = nextId;

            try
            {
                using var stream = File.Open(DataPaths.StockPath, FileMode.Append);
                using var writer = new StreamWriter(stream);
                using var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture);

                csvWriter.Configuration.RegisterClassMap<CsvStockMap>();
                csvWriter.Configuration.Delimiter = ";";

                csvWriter.WriteRecord(stockItem);
                csvWriter.NextRecord();

            }

            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message.ToString());
            }

            return stockItem;
        }

        /// <summary>
        /// Method removes item by id from stock
        /// </summary>
        /// <param name="id">id of a stock item to remove</param>
        /// <returns>true, if item id was found and removed from stock, false if not</returns>
        public bool Delete(int id)
        {
            bool found = false;
            try
            {
                using var reader = new StreamReader(DataPaths.StockPath, Encoding.Default);
                using var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);

                using var writer = new StreamWriter(DataPaths.StockPath + "temp", false, Encoding.Default);
                using var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture);

                csvReader.Configuration.RegisterClassMap<CsvStockMap>();
                csvReader.Configuration.Delimiter = ";";
                StockItem stockItem;
                
                csvWriter.Configuration.RegisterClassMap<CsvStockMap>();
                csvWriter.Configuration.Delimiter = ";";
                csvWriter.WriteHeader<StockItem>();
                csvWriter.NextRecord();

                while (csvReader.Read())
                {
                    stockItem = csvReader.GetRecord<StockItem>();
                    if (stockItem.Id == id)
                    {
                        found = true;
                    }
                    else
                    {
                        csvWriter.WriteRecord(stockItem);
                        csvWriter.NextRecord();
                    }
                }
            }

            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message.ToString());
            }

            File.Delete(DataPaths.StockPath); // Delete the existing file if exists
            File.Move(DataPaths.StockPath + "temp", DataPaths.StockPath); // Rename file name

            return found;
        }

        /// <summary>
        /// Method finds all stock items
        /// </summary>
        /// <returns>all items in stock</returns>
        public HashSet<StockItem> FindAll()
        {
            try
            {
                using var reader = new StreamReader(DataPaths.StockPath, Encoding.Default);
                using var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);

                csvReader.Configuration.RegisterClassMap<CsvStockMap>();
                csvReader.Configuration.Delimiter = ";";

                return new HashSet<StockItem>(csvReader.GetRecords<StockItem>());
            }

            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message.ToString());
            }

            return null;
        }

        /// <summary>
        /// Method finds stock item by id
        /// </summary>
        /// <param name="id">id of a stock item to find</param>
        /// <returns>stock item if id exists, null if item id was not found</returns>
        public StockItem FindById(int id)
        {
            try
            {
                using var reader = new StreamReader(DataPaths.StockPath, Encoding.Default);
                using var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);

                csvReader.Configuration.RegisterClassMap<CsvStockMap>();
                csvReader.Configuration.Delimiter = ";";
                StockItem stockItem;

                while (csvReader.Read())
                {
                    stockItem = csvReader.GetRecord<StockItem>();
                    if (stockItem.Id == id)
                    {
                        return stockItem;
                    }
                }
            }

            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message.ToString());
            }

            return null;
        }

        /// <summary>
        /// Method updates stock item by id
        /// </summary>
        /// <param name="id">id of a stock item to update</param>
        /// <param name="stockItem">stock item fields to update</param>
        /// <returns>updated stock item</returns>
        public StockItem Update(StockItem stockItem)
        {
            StockItem foundItem = null;
            try
            {
                using var reader = new StreamReader(DataPaths.StockPath, Encoding.Default);
                using var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);

                using var writer = new StreamWriter(DataPaths.StockPath + "temp", false, Encoding.Default);
                using var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture);

                csvReader.Configuration.RegisterClassMap<CsvStockMap>();
                csvReader.Configuration.Delimiter = ";";
                StockItem tempItem;

                csvWriter.Configuration.RegisterClassMap<CsvStockMap>();
                csvWriter.Configuration.Delimiter = ";";
                csvWriter.WriteHeader<StockItem>();
                csvWriter.NextRecord();

                while (csvReader.Read())
                {
                    tempItem = csvReader.GetRecord<StockItem>();
                    if (tempItem.Id == stockItem.Id)
                    {
                        foundItem = tempItem;

                        tempItem.Name = stockItem.Name;
                        tempItem.PortionCount = stockItem.PortionCount;
                        tempItem.PortionSize = stockItem.PortionSize;
                        tempItem.Unit = stockItem.Unit;
                    }
                    csvWriter.WriteRecord(tempItem);
                    csvWriter.NextRecord();
                }
            }

            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message.ToString());
            }

            File.Delete(DataPaths.StockPath); // Delete the existing file if exists
            File.Move(DataPaths.StockPath + "temp", DataPaths.StockPath); // Rename file name

            return foundItem;
        }
    }
}
