﻿using Agnė_Moleikaitytė_Task.Constants;
using Agnė_Moleikaitytė_Task.Data;
using Agnė_Moleikaitytė_Task.Mappers;
using Agnė_Moleikaitytė_Task.Repositories.IRepositories;
using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace Agnė_Moleikaitytė_Task.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        /// <summary>
        /// Method adds new order
        /// </summary>
        /// <param name="order">order to add</param>
        /// <returns>added order</returns>
        public Order Add(Order order)
        {
            var nextId = FindAll().Max(x => x.Id) + 1;
            order.Id = nextId;

            try
            {
                using var stream = File.Open(DataPaths.OrdersPath, FileMode.Append);
                using var writer = new StreamWriter(stream);
                using var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture);

                csvWriter.Configuration.RegisterClassMap<CsvOrderMap>();
                csvWriter.Configuration.Delimiter = ";";
                csvWriter.Configuration.ShouldQuote = (field, context) => false;

                csvWriter.NextRecord();
                csvWriter.WriteRecord(order);
            }

            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message.ToString());
            }

            return order;
        }

        /// <summary>
        /// Method removes order by id
        /// </summary>
        /// <param name="id">id of a order to remove</param>
        /// <returns>true, if order id was found and removed, false if not</returns>
        public bool Delete(int id)
        {
            bool found = false;
            try
            {
                using var reader = new StreamReader(DataPaths.OrdersPath, Encoding.Default);
                using var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);

                using var writer = new StreamWriter(DataPaths.OrdersPath + "temp", false, Encoding.Default);
                using var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture);

                csvReader.Configuration.RegisterClassMap<CsvOrderMap>();
                csvReader.Configuration.Delimiter = ";";
                Order order;

                csvWriter.Configuration.RegisterClassMap<CsvOrderMap>();
                csvWriter.Configuration.Delimiter = ";";
                csvWriter.WriteHeader<Order>();

                while (csvReader.Read())
                {
                    order = csvReader.GetRecord<Order>();
                    if (order.Id == id)
                    {
                        found = true;
                    }
                    else
                    {
                        csvWriter.NextRecord();
                        csvWriter.WriteRecord(order);
                    }
                }
            }

            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message.ToString());
            }

            File.Delete(DataPaths.OrdersPath); // Delete the existing file if exists
            File.Move(DataPaths.OrdersPath + "temp", DataPaths.OrdersPath); // Rename file name

            return found;
        }

        /// <summary>
        /// Method finds all orders
        /// </summary>
        /// <returns>all orders</returns>
        public HashSet<Order> FindAll()
        {
            try
            {
                using var reader = new StreamReader(DataPaths.OrdersPath, Encoding.Default);
                using var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);

                csvReader.Configuration.RegisterClassMap<CsvOrderMap>();
                csvReader.Configuration.Delimiter = ";";

                return new HashSet<Order>(csvReader.GetRecords<Order>());
            }

            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message.ToString());
            }

            return null;
        }

        /// <summary>
        /// Method finds order by id
        /// </summary>
        /// <param name="id">id of a order to find</param>
        /// <returns>order if id exists, null if order id was not found</returns>
        public Order FindById(int id)
        {
            try
            {
                using var reader = new StreamReader(DataPaths.OrdersPath, Encoding.Default);
                using var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);

                csvReader.Configuration.RegisterClassMap<CsvOrderMap>();
                csvReader.Configuration.Delimiter = ";";
                Order order;

                while (csvReader.Read())
                {
                    order = csvReader.GetRecord<Order>();
                    if (order.Id == id)
                    {
                        return order;
                    }
                }
            }

            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message.ToString());
            }

            return null;
        }

        /// <summary>
        /// Method updates order by id
        /// </summary>
        /// <param name="id">id of a order to update</param>
        /// <param name="order">order fields to update</param>
        /// <returns>updated order</returns>
        public Order Update(Order order)
        {
            Order foundOrder = null;
            try
            {
                using var reader = new StreamReader(DataPaths.OrdersPath, Encoding.Default);
                using var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);

                using var writer = new StreamWriter(DataPaths.OrdersPath + "temp", false, Encoding.Default);
                using var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture);

                csvReader.Configuration.RegisterClassMap<CsvOrderMap>();
                csvReader.Configuration.Delimiter = ";";
                Order tempOrder;

                csvWriter.Configuration.RegisterClassMap<CsvOrderMap>();
                csvWriter.Configuration.Delimiter = ";";
                csvWriter.WriteHeader<Order>();

                while (csvReader.Read())
                {
                    tempOrder = csvReader.GetRecord<Order>();
                    if (tempOrder.Id == order.Id)
                    {
                        foundOrder = tempOrder;
                        tempOrder.MenuItemIds = order.MenuItemIds;
                    }
                    csvWriter.NextRecord();
                    csvWriter.WriteRecord(tempOrder);
                }
            }

            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message.ToString());
            }

            File.Delete(DataPaths.OrdersPath); // Delete the existing file if exists
            File.Move(DataPaths.OrdersPath + "temp", DataPaths.OrdersPath); // Rename file name

            return foundOrder;
        }
    }
}
