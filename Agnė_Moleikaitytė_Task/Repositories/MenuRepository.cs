﻿using Agnė_Moleikaitytė_Task.Constants;
using Agnė_Moleikaitytė_Task.Data;
using Agnė_Moleikaitytė_Task.Mappers;
using Agnė_Moleikaitytė_Task.Repositories.IRepositories;
using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace Agnė_Moleikaitytė_Task.Repositories
{
    public class MenuRepository : IMenuRepository
    {
        /// <summary>
        /// Method adds new menu item
        /// </summary>
        /// <param name="menuItem">menu item to add</param>
        /// <returns>added menu item</returns>
        public MenuItem Add(MenuItem menuItem)
        {
            var nextId = FindAll().Max(x => x.Id) + 1;
            menuItem.Id = nextId;

            try
            {
                using var stream = File.Open(DataPaths.MenuPath, FileMode.Append);
                using var writer = new StreamWriter(stream);
                using var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture);

                csvWriter.Configuration.RegisterClassMap<CsvMenuItemMap>();
                csvWriter.Configuration.Delimiter = ";";
                csvWriter.Configuration.ShouldQuote = (field, context) => false;

                csvWriter.NextRecord();
                csvWriter.WriteRecord(menuItem);

            }

            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message.ToString());
            }

            return menuItem;
        }

        /// <summary>
        /// Method removes item by id from menu
        /// </summary>
        /// <param name="id">id of a menu item to remove</param>
        /// <returns>true, if item id was found and removed from menu, false if not</returns>
        public bool Delete(int id)
        {
            bool found = false;
            try
            {
                using var reader = new StreamReader(DataPaths.MenuPath, Encoding.Default);
                using var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);

                using var writer = new StreamWriter(DataPaths.MenuPath + "temp", false, Encoding.Default);
                using var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture);

                csvReader.Configuration.RegisterClassMap<CsvMenuItemMap>();
                csvReader.Configuration.Delimiter = ";";
                MenuItem menuItem;

                csvWriter.Configuration.RegisterClassMap<CsvMenuItemMap>();
                csvWriter.Configuration.Delimiter = ";";
                csvWriter.WriteHeader<MenuItem>();

                while (csvReader.Read())
                {
                    menuItem = csvReader.GetRecord<MenuItem>();
                    if (menuItem.Id == id)
                    {
                        found = true;
                    }
                    else
                    {
                        csvWriter.NextRecord();
                        csvWriter.WriteRecord(menuItem);
                    }
                }
            }

            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message.ToString());
            }

            File.Delete(DataPaths.MenuPath); // Delete the existing file if exists
            File.Move(DataPaths.MenuPath + "temp", DataPaths.MenuPath); // Rename file name

            return found;
        }

        /// <summary>
        /// Method finds all menu items
        /// </summary>
        /// <returns>all items in menu</returns>
        public HashSet<MenuItem> FindAll()
        {
            try
            {
                using var reader = new StreamReader(DataPaths.MenuPath, Encoding.Default);
                using var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);

                csvReader.Configuration.RegisterClassMap<CsvMenuItemMap>();
                csvReader.Configuration.Delimiter = ";";

                return new HashSet<MenuItem>(csvReader.GetRecords<MenuItem>());
            }

            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message.ToString());
            }

            return null;
        }

        /// <summary>
        /// Method finds menu item by id
        /// </summary>
        /// <param name="id">id of a menu item to find</param>
        /// <returns>menu item if id exists, null if item id was not found</returns>
        public MenuItem FindById(int id)
        {
            try
            {
                using var reader = new StreamReader(DataPaths.MenuPath, Encoding.Default);
                using var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);

                csvReader.Configuration.RegisterClassMap<CsvMenuItemMap>();
                csvReader.Configuration.Delimiter = ";";
                MenuItem menuItem;

                while (csvReader.Read())
                {
                    menuItem = csvReader.GetRecord<MenuItem>();
                    if (menuItem.Id == id)
                    {
                        return menuItem;
                    }
                }
            }

            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message.ToString());
            }

            return null;
        }

        /// <summary>
        /// Method updates menu item by id
        /// </summary>
        /// <param name="id">id of a menu item to update</param>
        /// <param name="menuItem">menu item fields to update</param>
        /// <returns>updated menu item</returns>
        public MenuItem Update(int id, MenuItem menuItem)
        {
            MenuItem foundItem = null;
            try
            {
                using var reader = new StreamReader(DataPaths.MenuPath, Encoding.Default);
                using var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);

                using var writer = new StreamWriter(DataPaths.MenuPath + "temp", false, Encoding.Default);
                using var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture);

                csvReader.Configuration.RegisterClassMap<CsvMenuItemMap>();
                csvReader.Configuration.Delimiter = ";";
                MenuItem tempItem;

                csvWriter.Configuration.RegisterClassMap<CsvMenuItemMap>();
                csvWriter.Configuration.Delimiter = ";";
                csvWriter.WriteHeader<MenuItem>();

                while (csvReader.Read())
                {
                    tempItem = csvReader.GetRecord<MenuItem>();
                    if (tempItem.Id == id)
                    {
                        foundItem = tempItem;

                        tempItem.Name = menuItem.Name;
                        tempItem.ProductIds = menuItem.ProductIds;
                    }
                    csvWriter.NextRecord();
                    csvWriter.WriteRecord(tempItem);
                }
            }

            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message.ToString());
            }

            File.Delete(DataPaths.MenuPath); // Delete the existing file if exists
            File.Move(DataPaths.MenuPath + "temp", DataPaths.MenuPath); // Rename file name

            return foundItem;
        }
    }
}
