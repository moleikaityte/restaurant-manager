﻿using Agnė_Moleikaitytė_Task.Data;
using System.Collections.Generic;

namespace Agnė_Moleikaitytė_Task.Repositories.IRepositories
{
    public interface IStockRepository
    {
        public StockItem Add(StockItem stockItem);
        public StockItem FindById(int id);
        public HashSet<StockItem> FindAll();
        public bool Delete(int id);
        public StockItem Update(StockItem stockItem);
    }
}
