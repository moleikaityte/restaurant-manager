﻿using Agnė_Moleikaitytė_Task.Data;
using System.Collections.Generic;

namespace Agnė_Moleikaitytė_Task.Repositories.IRepositories
{
    public interface IMenuRepository
    {
        public MenuItem Add(MenuItem menuItem);
        public MenuItem FindById(int id);
        public HashSet<MenuItem> FindAll();
        public bool Delete(int id);
        public MenuItem Update(int id, MenuItem stockItem);
    }
}
