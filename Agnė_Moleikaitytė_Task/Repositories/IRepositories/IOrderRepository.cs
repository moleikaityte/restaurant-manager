﻿using Agnė_Moleikaitytė_Task.Data;
using System.Collections.Generic;

namespace Agnė_Moleikaitytė_Task.Repositories.IRepositories
{
    public interface IOrderRepository
    {
        public Order Add(Order order);
        public Order FindById(int id);
        public HashSet<Order> FindAll();
        public bool Delete(int id);
        public Order Update(Order order);
    }
}
