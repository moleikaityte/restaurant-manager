﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Agnė_Moleikaitytė_Task.Data
{
    public class MenuItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ProductIds { get; set; }

        public void SetProductIds(HashSet<int> ids)
        {
            ProductIds =  string.Join(' ', ids);
        }

        public override string ToString()
        {
            return string.Format("|{0,-3}|{1,-30}|{2,11}|", Id, Name, ProductIds);
        }
    }

}
