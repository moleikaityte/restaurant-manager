﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Agnė_Moleikaitytė_Task.Data
{
    public class Order
    {
        public int Id { get; set; }
        public DateTime DateTime { get; set; }
        public string MenuItemIds { get; set; }

        public void SetMenuItemIds(IList<int> ids)
        {
            MenuItemIds = string.Join(' ', ids);
        }

        public override string ToString()
        {
            return string.Format("|{0,-3}|{1,-20}|{2,13}|", Id, DateTime, MenuItemIds);
        }
    }
}
