﻿namespace Agnė_Moleikaitytė_Task.Data
{
    public class StockItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double PortionCount { get; set; }
        public EUnit Unit { get; set; }
        public double PortionSize { get; set; }

        public override string ToString()
        {
            return string.Format("|{0,-3}|{1,-20}|{2,-13}|{3,-5}|{4,-12}|", Id, Name, PortionCount, Unit, PortionSize);
        }

        public override int GetHashCode()
        {
            return Id;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as StockItem);
        }

        public bool Equals(StockItem obj)
        {
            return obj != null && obj.Id == this.Id;
        }
    }
}
