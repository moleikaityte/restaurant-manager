﻿namespace Agnė_Moleikaitytė_Task.Constants
{
    public static class CsvStockHeaders
    {
        public const string Id = "Id";
        public const string Name = "Name";
        public const string PortionCount = "Portion Count";
        public const string Unit = "Unit";
        public const string PortionSize = "Portion size";
        public const string PortionIsIndivisible = "Indivisible";
    }
}
