﻿namespace Agnė_Moleikaitytė_Task.Constants
{
    public static class CsvOrderHeaders
    {
        public const string Id = "Id";
        public const string DateTime = "DateTime";
        public const string MenuItems = "Menu Items";
    }
}
