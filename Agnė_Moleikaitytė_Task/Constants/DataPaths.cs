﻿namespace Agnė_Moleikaitytė_Task.Constants
{
    public static class DataPaths
    {
        public const string MenuPath = "../../../Data/menu.csv";
        public const string OrdersPath = "../../../Data/orders.csv";
        public const string StockPath = "../../../Data/stock.csv";
    }
}
