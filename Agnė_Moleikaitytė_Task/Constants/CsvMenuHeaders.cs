﻿namespace Agnė_Moleikaitytė_Task.Constants
{
    public static class CsvMenuHeaders
    {
        public const string Id = "Id";
        public const string Name = "Name";
        public const string Products = "Products";
    }
}
