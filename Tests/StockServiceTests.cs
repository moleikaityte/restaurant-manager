﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Agnė_Moleikaitytė_Task.Data;
using Agnė_Moleikaitytė_Task.Services.IServices;
using Agnė_Moleikaitytė_Task.Services;
using Agnė_Moleikaitytė_Task.Repositories.IRepositories;
using Agnė_Moleikaitytė_Task.Repositories;

namespace Tests
{
    public class StockServiceTests
    {
        private static readonly IStockRepository stockRepository = new StockRepository();
        private static readonly StockService stockService = new StockService(stockRepository);

        [Fact]
        public void IsEnoughStockForOrder_ShouldReturnFalse()
        {
            var stockItem = new StockItem
            {
                Name = "newStockItem",
                PortionCount = 1,
                PortionSize = 0.2,
                Unit = EUnit.kg
            };

            var stockItems = new Dictionary<StockItem, int>();
            stockItems.Add(stockItem, 2);

            var enoughStock = stockService.IsEnoughStockForOrder(stockItems);

            Assert.False(enoughStock);
        }

        [Fact]
        public void IsEnoughStockForOrder_ShouldReturnTrue()
        {
            var stockItem = new StockItem
            {
                Name = "newStockItem",
                PortionCount = 1,
                PortionSize = 0.2,
                Unit = EUnit.kg
            };

            var stockItems = new Dictionary<StockItem, int>();
            stockItems.Add(stockItem, 1);

            var enoughStock = stockService.IsEnoughStockForOrder(stockItems);

            Assert.True(enoughStock);
        }

        [Fact]
        public void FindById_ShouldReturnFalse()
        {
            var id = -1;
            var item = stockService.FindById(id);

            Assert.Null(item);
        }


    }
}
