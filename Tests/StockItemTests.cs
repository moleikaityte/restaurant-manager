﻿using Agnė_Moleikaitytė_Task.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Tests
{
    public class StockItemTests
    {
        [Fact]
        public void Equals_ShouldReturnTrue()
        {
            var stockItem1 = new StockItem
            {
                Id = 1,
                Name = "newStockItem",
                PortionCount = 10,
                PortionSize = 0.2,
                Unit = EUnit.kg
            };

            var stockItem2 = new StockItem
            {
                Id = 1,
                Name = "newStockItem",
                PortionCount = 1,
                PortionSize = 0.2,
                Unit = EUnit.kg
            };

            var result = stockItem1.Equals(stockItem2);

            Assert.True(result);
        }

        [Fact]
        public void Equals_ShouldReturnFalse()
        {
            var stockItem1 = new StockItem
            {
                Id = 2,
                Name = "newStockItem",
                PortionCount = 10,
                PortionSize = 0.2,
                Unit = EUnit.kg
            };

            var stockItem2 = new StockItem
            {
                Id = 1,
                Name = "newStockItem",
                PortionCount = 1,
                PortionSize = 0.2,
                Unit = EUnit.kg
            };

            var result = stockItem1.Equals(stockItem2);

            Assert.False(result);
        }
    }
}
